//console.log("Hello");

const getCube = 2 ** 3
console.log(`The cube of 2 is ${getCube}`);

// -------------

const address = ["258 Washington Ave NW,", "Califronia 90011",];

const [street, state] = address;

console.log(`I live at ${street} ${state}`);

// -------------

const animal = {
	crocodileName: "Lolong",
	type: "saltwater",
	weight: "1075",
	height: "20 ft 3 in"
}
const {crocodileName,type, weight, height} = animal;

console.log(`${crocodileName} was a ${type} crocodile. He weighed at ${weight} kgs with a measurement of ${height}.`);

// -------------

const numbers = [1,2,3,4,5];

numbers.forEach((numbers) => {
	console.log(`${numbers}`);
})


let reducedNumber = numbers.reduce((acc,cur) => {
    return acc + cur;
});

console.log(reducedNumber);
;



// -------------

class Dog {
	contructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const newDog = new Dog();

newDog.name = "Frankie";
newDog.age = 5;
newDog.breed= "Miniature Dachschund";
console.log(newDog);
